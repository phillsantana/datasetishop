const express 		= require('express');
const bodyParser 	= require('body-parser');
const cors 			= require('cors');
const helmet 		= require('helmet');
const morgan 		= require('morgan');

const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));

var dataArray = [{
	"nome": "Adriana Calcanhoto",
	"email": "adriana@calcanhoto.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "813.418.770-60",
	"cnpj": "58.797.811/0001-45",
	"cep": "49069027",
	"numero": "151"

},

{
	"nome": "Ana Paula",
	"email": "ana@paula.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "975.342.990-80",
	"cnpj": "61.264.254/0001-01",
	"cep": "65911580",
	"numero": "151"
},

{
	"nome": "Maria do Carmo",
	"email": "mariado@carmo.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "493.861.750-10",
	"cnpj": "03.682.367/0001-20",
	"cep": "80040380",
	"numero": "151"
},

{
	"nome": "Sandra Oliveira",
	"email": "sandra@oliveira.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "",
	"cnpj": "03.682.367/0001-20",
	"cep": "80040380",
	"numero": "151"
},

{
	"nome": "Juliana Prado",
	"email": "juliana@prado.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "923.497.640-10",
	"cnpj": "02.325.551/0001-50",
	"cep": "58301115",
	"numero": "151"
},

{
	"nome": "Antônio Bandeiras",
	"email": "antônio@bandeiras.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "913.939.180-93",
	"cnpj": "54.319.650/0001-23",
	"cep": "56906371",
	"numero": "151"
},

{
	"nome": "Carlos Alberto",
	"email": "carlos@alberto.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "391.647.920-21",
	"cnpj": "56.481.182/0001-14",
	"cep": "86082-390",
	"numero": "151"
},

{
	"nome": "Francisco Silva",
	"email": "francisco@silva.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "104.747.930-36",
	"cnpj": "66.460.042/0001-13",
	"cep": "76801-234",
	"numero": "151"
},

{
	"nome": "João Cunha",
	"email": "joao@cunha.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "469.113.100-08",
	"cnpj": "66.236.688/0001-11",
	"cep": "56322-710",
	"numero": "151"
},

{
	"nome": "José da Conceição",
	"email": "joseda@conceicao.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "249.496.480-63",
	"cnpj": "49.958.037/0001-16",
	"cep": "68900-075",
	"numero": "151"
},

{
	"nome": "Bruna Oliveira",
	"email": "bruna@oliveira.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "529.381.650-05",
	"cnpj": "39.473.714/0001-52",
	"cep": "69919-870",
	"numero": "151"
},

{
	"nome": "Camila Pitanga",
	"email": "camila@pitanga.com",
	"senha": "12345",
	"telefone": "11 4002-8922",
	"cpf": "185.065.130-22",
	"cnpj": "33.088.726/0001-87",
	"cep": "85858-551",
	"numero": "151"
}];

app.get('/', (request,response) => {
	if(dataArray.length){
		let dataResponse = dataArray.shift();
		dataArray.push(dataResponse);
		return response.status(200).send(dataResponse);
	}
	else{
		return response.send('No data yet');
	}
});

app.post('/', (request, response) => {
	console.log(
		'\nBody:',request.body
	);

	let dataRequest = request.body;

	dataArray.push(dataRequest);

	response.status(200).send(JSON.stringify({status: 'OK'}));
});

app.listen('3000',() => console.log('Listening on port 3000'));