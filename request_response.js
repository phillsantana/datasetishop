// POST
fetch('http://localhost:3000/', {
	method: 'POST',
	headers: {
		// "Content-Type":"application/json"
		"Content-Type":"text/plain"
	},
	body: '<main><h1>Teste</h1></main>'
});


// GET
fetch('http://localhost:3000/', {
	method: 'GET'
})
.then(response => response.json())
.then(response => {
		response.forEach(doc => {
			let responseDoc = new DOMParser().parseFromString(doc,'text/html');
			responseDoc = responseDoc.body.firstElementChild;
			
			document.body.appendChild(responseDoc);
		});
});